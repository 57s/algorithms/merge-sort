function mergeSort(array) {
	if (array.length > 1) {
		const left = mergeSort(array.slice(0, array.length / 2));
		const right = mergeSort(array.slice(array.length / 2, array.length));
		let i = 0;
		let j = 0;
		const sorted_array = [];

		while (i < left.length && j < right.length) {
			if (left[i] === right[j]) {
				sorted_array.push(left[i], right[j]);
				i++;
				j++;
			} else if (left[i] < right[j]) {
				sorted_array.push(left[i]);
				i++;
			} else {
				sorted_array.push(right[j]);
				j++;
			}
		}

		if (i !== left.length) {
			return sorted_array.concat(left.slice(i, left.length));
		} else if (j !== right.length) {
			return sorted_array.concat(right.slice(j, right.length));
		}

		return sorted_array;
	}

	return array;
}

export { mergeSort };
