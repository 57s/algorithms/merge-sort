function medianFromArrays(arr1, arr2) {
	function mergeSort(arr) {
		if (arr.length < 2) return arr;

		const mid = Math.floor(arr.length / 2);
		const left = arr.slice(0, mid);
		const right = arr.slice(mid);

		return merge(mergeSort(left), mergeSort(right));
	}

	function merge(left, right) {
		const result = [];

		while ((left.length, right.length)) {
			if (left[0] < right[0]) {
				result.push(left.shift());
			} else {
				result.push(right.shift());
			}
		}

		return result.concat(left, right);
	}

	const sorted = mergeSort([...arr1, ...arr2]);
	let median;

	if (sorted.length % 2) {
		const idx = Math.floor(sorted.length / 2);
		median = sorted[idx];
	} else {
		const idxR = sorted.length / 2;
		const idxL = idxR - 1;
		median = (sorted[idxL] + sorted[idxR]) / 2;
	}

	return median;
}

export { medianFromArrays };
