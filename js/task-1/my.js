function medianFromArray(array) {
	function mergeSort(array) {
		if (array.length < 2) return array;

		const middle = Math.floor(array.length / 2);
		const left = array.slice(0, middle);
		const right = array.slice(middle);

		function merge(left, right) {
			let result = [];

			while (left.length && right.length) {
				if (left[0] > right[0]) {
					result.push(right.shift());
				} else {
					result.push(left.shift());
				}
			}

			return result.concat(left, right);
		}

		return merge(mergeSort(left), mergeSort(right));
	}

	let sortArray = mergeSort(array);

	const middleIndex = Math.floor(sortArray.length / 2);

	if (sortArray.length & (2 !== 0)) {
		const leftIndex = middleIndex;
		const rightIndex = middleIndex + 1;

		return (sortArray[leftIndex] + sortArray[rightIndex]) / 2;
	} else {
		return sortArray.at(middleIndex);
	}
}

export { medianFromArray };
