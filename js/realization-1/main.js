function mergeSort(array) {
	const { length } = array;
	if (length < 2) return array;

	const mid = Math.floor(length / 2);
	const left = array.slice(0, mid);
	const right = array.slice(mid);

	function merge(left, right) {
		let result = [];

		while (left.length && right.length) {
			if (left[0] < right[0]) {
				result.push(left.shift());
			} else {
				result.push(right.shift());
			}
		}

		return result.concat(left, right);
	}

	return merge(mergeSort(left), mergeSort(right));
}

export { mergeSort };
