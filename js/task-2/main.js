function dateConversion(dataString) {
	const dataArray = dataString.split('.');
	const dataArrayReverse = [...dataArray].reverse();
	const dataNewString = dataArrayReverse.join('-');
	const dataObject = new Date(dataNewString);

	console.group('Преобразование даты');
	console.log(`Входящая строка: ${dataString}`);
	console.log(`Массив из строки: [${dataArray}]`);
	console.log(`Развернутый массив: [${dataArrayReverse}]`);
	console.log(`Новая строка с датой: ${dataNewString}`);
	console.log(`Объект типа Date: ${dataObject}`);
	console.groupEnd();

	return dataObject;
}

function sortDates(array) {
	if (array.length < 2) return array;

	const mid = Math.floor(array.length / 2);
	const left = array.slice(0, mid);
	const right = array.slice(mid);

	function merge(left, right) {
		const result = [];

		while (left.length && right.length) {
			// Преобразование даты для дальнейшего сравнения
			// 10.02.2020 > 2020-02-10
			const leftData = dateConversion(left[0]);
			const rightData = dateConversion(right[0]);

			if (leftData < rightData) {
				result.push(left.shift());
			} else {
				result.push(right.shift());
			}
		}

		return result.concat(left, right);
	}

	return merge(sortDates(left), sortDates(right));
}

export { sortDates };
