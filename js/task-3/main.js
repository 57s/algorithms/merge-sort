function getSortAdmins(arr) {
	if (arr.length < 2) return arr;

	const mid = Math.floor(arr.length / 2);
	const left = arr.slice(0, mid);
	const right = arr.slice(mid);

	function merge(left, right) {
		const result = [];
		while (left.length && right.length) {
			if (left[0].age < right[0].age) {
				result.push(left.shift());
			} else {
				result.push(right.shift());
			}
		}
		return result.concat(left, right);
	}

	const sorted = merge(getSortAdmins(left), getSortAdmins(right));
	const result = sorted.filter(person => person.isAdmin);

	return result;
}

export { getSortAdmins };
