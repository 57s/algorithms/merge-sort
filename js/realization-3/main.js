function mergeSort(array) {
	if (array.length > 1) {
		const mid = Math.floor(array.length / 2);
		const lefthalf = array.slice(0, mid);
		const righthalf = array.slice(mid);

		// Разложить массив на части
		mergeSort(lefthalf);
		mergeSort(righthalf);

		// Индекс левой части
		let i = 0;
		// Индекс правой части части
		let j = 0;
		// Индекс результата
		let k = 0;

		//  Оптимизация памяти, не создается массив с результатом
		while (i < lefthalf.length && j < righthalf.length) {
			if (lefthalf[i] < righthalf[j]) {
				array[k] = lefthalf[i];
				i++;
			} else {
				array[k] = righthalf[j];
				j++;
			}
			k++;
		}

		// Если в левой части остались элементы, добавить их
		while (i < lefthalf.length) {
			array[k] = lefthalf[i];
			i++;
			k++;
		}

		// Если в правой части остались элементы, добавить их
		while (j < righthalf.length) {
			array[k] = righthalf[j];
			j++;
			k++;
		}
	}

	return array;
}

export { mergeSort };
